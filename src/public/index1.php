<?php
/**
 * Created by PhpStorm.
 * User: arnajonasar
 */

require 'data/dbcon.php';

?>
<!doctype HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pic upload</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
<div class="container">
    <div id="uploadForm" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <form action="upload.php" method="post" enctype="multipart/form-data">
            Select image to upload:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit" value="Upload Image" name="submit">
        </form>
    </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>
